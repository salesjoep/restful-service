package joep.sem3.service.resources;

import joep.sem3.service.model.*;
import joep.sem3.service.repository.*;
import joep.sem3.service.model.Shop;
import joep.sem3.service.model.Sweater;
import joep.sem3.service.repository.FakeDataStore;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/sweaters")
public class SweaterResources {
    @Context
    private UriInfo uriInfo;
    // this has to be static because the service is stateless:
    private static final FakeDataStore fakeDataStore = new FakeDataStore();

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public Response sayHello() {
        String msg = " Hello, your resources works!";
        return Response.ok(msg).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSweaterPath(@PathParam("id") int stNr) {
        Sweater sweater = fakeDataStore.getSweater(stNr);
        if (sweater == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid sweater number.").build();
        } else {
            return Response.ok(sweater).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllSweaters(@QueryParam("shop") String shopCode) {
        List<Sweater> sweaters;
        if (uriInfo.getQueryParameters().containsKey("shop")){
            Shop shop = fakeDataStore.getShop(shopCode);
            if (shop == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop code.").build();
            } else {
                sweaters = fakeDataStore.getSweater(shop);
            }
        } else {
            sweaters = fakeDataStore.getSwearerList();
        }
        GenericEntity<List<Sweater>> entity = new GenericEntity<>(sweaters) {  };
        return Response.ok(entity).build();
    }


    @DELETE
    @Path("{id}")
    public Response deleteSweater(@PathParam("id") int stNr) {
        fakeDataStore.deleteSweater(stNr);
        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createSweater(Sweater sweater) {
        if (!fakeDataStore.addSweater(sweater)){
            String entity =  "Sweater with student number " + sweater.getSweaterNumber() + " already exists.";
            return Response.status(Response.Status.CONFLICT).entity(entity).build();
        } else {
            String url = uriInfo.getAbsolutePath() + "/" + sweater.getSweaterNumber();
            URI uri = URI.create(url);
            return Response.created(uri).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response updateSweater(Sweater sweater) {
        if (fakeDataStore.updateSweater(sweater)) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid sweater number.").build();
        }
    }


    @PUT
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Path("{id}")
    public Response updateSweater(@PathParam("id") int stNr,  @FormParam("name") String name, @FormParam("shop") String shopCode) {
        Sweater sweater = fakeDataStore.getSweater(stNr);
        if (sweater == null){
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid sweater number.").build();
        }

        Shop shop = fakeDataStore.getShop(shopCode);
        if (shop == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop code.").build();
        }

        sweater.setSweaterName(name);
        sweater.setShop(shop);
        return Response.noContent().build();
    }
}
