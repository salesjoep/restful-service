package joep.sem3.service.resources;

import joep.sem3.service.repository.FakeDataStore;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

import joep.sem3.service.model.Shop;

@Path("/shops")
public class ShopResources {
    @Context
    private UriInfo uriInfo;
    private static final FakeDataStore fakeDataStore = new FakeDataStore();

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public Response sayHello() {
        String msg = " Hello, your resources works!";
        return Response.ok(msg).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getShopPath(@PathParam("id") String stNr) {
        Shop shop = fakeDataStore.getShop(stNr);
        if (shop == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop ID.").build();
        } else {
            return Response.ok(shop).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllShops(@QueryParam("shop") String shopCode) {
        List<Shop> shops;
        if (uriInfo.getQueryParameters().containsKey("shop")){
            Shop shop = fakeDataStore.getShop(shopCode);
            if (shop == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop code.").build();
            } else {
                shops = fakeDataStore.getShopList();
            }
        } else {
            shops = fakeDataStore.getShopList();
        }
        GenericEntity<List<Shop>> entity = new GenericEntity<>(shops) {  };
        return Response.ok(entity).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteShop(@PathParam("id") String code) {
        fakeDataStore.deleteShop(code);
        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createShop(Shop shop) {
        if (!fakeDataStore.addShop(shop)){
            String entity =  "Shop with code " + shop.getCode() + " already exists.";
            return Response.status(Response.Status.CONFLICT).entity(entity).build();
        } else {
            String url = uriInfo.getAbsolutePath() + "/" + shop.getCode();
            URI uri = URI.create(url);
            return Response.created(uri).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response updateShop(Shop shop) {
        if (fakeDataStore.updateShop(shop)) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid shop ID.").build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Path("{id}")
    public Response UpdateShop(@PathParam("id") String code,  @FormParam("name") String name, @FormParam("shop") String shopCode) {
        Shop shop = fakeDataStore.getShop(code);
        if (shop == null){
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid shop ID.").build();
        }

        Shop _shop = fakeDataStore.getShop(shopCode);
        if (_shop == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop ID.").build();
        }

        shop.setName(name);
        shop.setCode(code);
        return Response.noContent().build();
    }
}
