package joep.sem3.service.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

import joep.sem3.service.model.Pants;
import joep.sem3.service.model.Shop;
import joep.sem3.service.repository.*;
import joep.sem3.service.repository.FakeDataStore;


@Path("/pants")
public class PantsResources {
    @Context
    private UriInfo uriInfo;
    private static final FakeDataStore fakeDataStore = new FakeDataStore();

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public Response sayHello() {
        String msg = " Hello, your resources works!";
        return Response.ok(msg).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPantsPath(@PathParam("id") int stNr) {
        Pants pants = fakeDataStore.getPants(stNr);
        if (pants == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid pants number.").build();
        } else {
            return Response.ok(pants).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPants(@QueryParam("shop") String shopCode) {
        List<Pants> pants;
        if (uriInfo.getQueryParameters().containsKey("shop")){
             Shop shop = fakeDataStore.getShop(shopCode);
            if (shop == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop code.").build();
            } else {
                pants = fakeDataStore.getPants(shop);
            }
        } else {
            pants = fakeDataStore.getPantsList();
        }
        GenericEntity<List<Pants>> entity = new GenericEntity<>(pants) {  };
        return Response.ok(entity).build();
    }

    @DELETE
    @Path("{id}")
    public Response deletePants(@PathParam("id") int code) {
        fakeDataStore.deletePants(code);
        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPants(Pants pants) {
        if (!fakeDataStore.addPants(pants)){
            String entity =  "Pants with code " + pants.getPantsNumber() + " already exists.";
            return Response.status(Response.Status.CONFLICT).entity(entity).build();
        } else {
            String url = uriInfo.getAbsolutePath() + "/" + pants.getPantsNumber();
            URI uri = URI.create(url);
            return Response.created(uri).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response updatePants(Pants pants) {
        if (fakeDataStore.updatePants(pants)) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid pants number.").build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Path("{id}")
    public Response updatePants(@PathParam("id") int code,  @FormParam("name") String name, @FormParam("shop") String shopCode) {
        Pants pants = fakeDataStore.getPants(code);
        if (pants == null){
            return Response.status(Response.Status.NOT_FOUND).entity("Please provide a valid pants number.").build();
        }

        Shop shop = fakeDataStore.getShop(shopCode);
        if (shop == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid shop code.").build();
        }

        pants.setPantsName(name);
        pants.setShop(shop);
        return Response.noContent().build();
    }
}
