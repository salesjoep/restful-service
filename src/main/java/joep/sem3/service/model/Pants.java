package joep.sem3.service.model;

import java.util.Objects;

public class Pants {
    private int pantsNumber;
    private String pantsName;
    private Shop shop;

    //Constructor
    public Pants(int pantsNumber, String pantsName, Shop shop){
        this.pantsNumber = pantsNumber;
        this.pantsName = pantsName;
        this.shop = shop;
    }

    //Empty Constructor
    public Pants(){

    }

    //Class Properties
    public int getPantsNumber(){return pantsNumber;}
    public void setPantsNumber(int pantsNumber){this.pantsNumber = pantsNumber;}

    public String getPantsName(){return pantsName;}
    public void setPantsName(String pantsName){this.pantsName = pantsName;}

    public Shop getShop(){return shop;}
    public void setShop(Shop shop){this.shop = shop;}

    //Class Methods
    public boolean equal(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pants pants = (Pants) o;
        return pantsNumber == pants.pantsNumber;
    }

    public int HashCode(){
        return Objects.hash(pantsNumber);
    }

    public String toString(){
        return "Sweater{" +
                "sweaterNumber = " + pantsNumber +
                ", sweaterName='" + pantsName + '\'' +
                ", shop = " + shop + '}';
    }
}
