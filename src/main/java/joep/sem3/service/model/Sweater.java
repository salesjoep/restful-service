package joep.sem3.service.model;

import java.util.Objects;

public class Sweater {
    private int sweaterNumber;
    private String sweaterName;
    private Shop shop;

    //Constructor
    public Sweater(int sweaterNumber, String sweaterName, Shop shop){
        this.sweaterNumber = sweaterNumber;
        this.sweaterName = sweaterName;
        this.shop = shop;
    }

    //Empty Constructor
    public Sweater(){

    }

    //Class Properties
    public int getSweaterNumber(){return sweaterNumber;}
    public void setSweaterNumber(int sweaterNumber){this.sweaterNumber = sweaterNumber;}

    public String getSweaterName(){return sweaterName;}
    public void setSweaterName(String sweaterName){this.sweaterName = sweaterName;}

    public Shop getShop(){return shop;}
    public void setShop(Shop shop){this.shop = shop;}

    //Class Methods
    public boolean equal(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sweater sweater = (Sweater) o;
        return sweaterNumber == sweater.sweaterNumber;
    }

    public int HashCode(){
        return Objects.hash(sweaterNumber);
    }

    public String toString(){
        return "Sweater{" +
                "sweaterNumber = " + sweaterNumber +
                ", sweaterName='" + sweaterName + '\'' +
                ", shop = " + shop + '}';
    }
}
