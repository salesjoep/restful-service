package joep.sem3.service.model;

import java.util.Objects;

public class Shop {

    private String code;
    private String name;

    //Class Constructor
    public Shop(String code, String name) {
        this.code = code;
        this.name = name;
    }

    //Empty Constructor
    public Shop(){

    }

    //Class Properties
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Class Methods
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return Objects.equals(code, shop.code);
    }

    public int hashCode() {
        return Objects.hash(code);
    }

    public String toString() {
        return "Shop{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
