package joep.sem3.service.repository;

import joep.sem3.service.model.*;
import joep.sem3.service.model.Pants;
import joep.sem3.service.model.Shop;
import joep.sem3.service.model.Sweater;

import java.util.ArrayList;
import java.util.List;

public class FakeDataStore {

    private final List<Shop> shopList = new ArrayList<>();
    private final List<Pants> pantsList = new ArrayList<>();
    private final List<Sweater> swearerList = new ArrayList<>();

    public FakeDataStore() {

        Shop zalando = new Shop("ZA", "Zalando Nederland");
        Shop hema = new Shop("HE", "Hema Nederland");
        Shop jackandjones = new Shop("JJ", "Jack & Jones");
        shopList.add(zalando);
        shopList.add(hema);
        shopList.add(jackandjones);

        swearerList.add(new Sweater(1, "Black Hoodie", zalando));
        swearerList.add(new Sweater(2, "White Sweater", hema));
        swearerList.add(new Sweater(3, "Red Sweater", jackandjones));
        swearerList.add(new Sweater(4, "Grey Hoodie", jackandjones));

        pantsList.add(new Pants(1, "Denim Jeans", hema));
        pantsList.add(new Pants(2, "Ripped Jeans", jackandjones));
        pantsList.add(new Pants(3, "Short Jeans", zalando));
        pantsList.add(new Pants(4, "Black Jeans", jackandjones));
        pantsList.add(new Pants(5, "Ripped Denim Jeans", hema));
    }

    public List<Pants> getPantsList() {
        return pantsList;
    }
    public List<Shop> getShopList() {
        return shopList;
    }
    public List<Sweater> getSwearerList(){return swearerList;}

    //Get All Pants
    public List<Pants> getPants(Shop shop) {
        List<Pants> filtered = new ArrayList<>();
        for (Pants pants : pantsList) {
            if (pants.getShop().equals(shop)) {
                filtered.add(pants);
            }
        }
        return filtered;
    }

    //Get All Sweaters
    public List<Sweater> getSweater(Shop shop) {
        List<Sweater> filtered = new ArrayList<>();
        for (Sweater sweater : swearerList) {
            if (sweater.getShop().equals(shop)) {
                filtered.add(sweater);
            }
        }
        return filtered;
    }

    //Get Pants By ID
    public Pants getPants(int nr) {
        for (Pants pants : pantsList) {
            if (pants.getPantsNumber() == nr)
                return pants;
        }
        return null;
    }

    //Get Sweater By ID
    public Sweater getSweater(int nr) {
        for (Sweater sweater : swearerList) {
            if (sweater.getSweaterNumber() == nr)
                return sweater;
        }
        return null;
    }

    //Delete Pants By ID
    public boolean deletePants(int stNr) {
        Pants pants = getPants(stNr);
        if (pants == null){
            return false;
        }

        return pantsList.remove(pants);
    }

    //Delete Sweater By ID
    public boolean deleteSweater(int stNr) {
        Sweater sweater = getSweater(stNr);
        if (sweater == null){
            return false;
        }

        return swearerList.remove(sweater);
    }

    //Delete Shop By Code
    public boolean deleteShop(String code) {
        Shop shop = getShop(code);
        if (shop == null){
            return false;
        }

        return shopList.remove(shop);
    }

    //Create New Pants
    public boolean addPants(Pants pants) {
        if (this.getPants(pants.getPantsNumber()) != null){
            return false;
        }
        pantsList.add(pants);
        return true;
    }

    //Create New Sweater
    public boolean addSweater(Sweater sweater){
        if (this.getSweater(sweater.getSweaterNumber()) != null){
            return false;
        }
        swearerList.add(sweater);
        return true;
    }

    public boolean addShop(Shop shop) {
        if (this.getShop(shop.getCode()) != null){
            return false;
        }
        shopList.add(shop);
        return true;
    }

    //Update Pants
    public boolean updatePants(Pants pants) {
        Pants old = this.getPants(pants.getPantsNumber());
        if (old == null) {
            return false;
        }
        old.setPantsName(pants.getPantsName());
        old.setShop(pants.getShop());
        return true;
    }

    //Update Sweater
    public boolean updateSweater(Sweater sweater) {
        Sweater old = this.getSweater(sweater.getSweaterNumber());
        if (old == null) {
            return false;
        }
        old.setSweaterName(sweater.getSweaterName());
        old.setShop(sweater.getShop());
        return true;
    }

    //Update Shop
    public boolean updateShop(Shop shop) {
        Shop old = this.getShop(shop.getCode());
        if (old == null) {
            return false;
        }
        old.setName(shop.getName());
        old.setCode(shop.getCode());

        return true;
    }

    public Shop getShop(String shopCode) {
        for (Shop shop : shopList) {
            if (shop.getCode().equals(shopCode)) {
                return shop;
            }
        }
        return null;
    }
}
